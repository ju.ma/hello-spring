The steps in this file assume that you have already provisioned the 
[kubedb operator](https://kubedb.com/docs/0.9.0/guides/postgres/quickstart/quickstart/) in your cluster.
It also assumes that all commands are executed from the root directory of 
the repository. 

* Create a new latest image by running `make release PREFIX=latest COMMIT_HASH=""`
* Create new namespace for apps `kubectl create ns apps`
* Create a new database by executing `kubectl apply -f manifests/database.yaml`
* Wait for the database to be Running. You can monitor by executing 
  `watch kubectl get postgres,pods -n apps`
* Create a new database in the newly provisioned database by running 
  `kubectl exec -it -n apps hello-spring-db-0 -- psql -U postgres -c 'CREATE database hello_spring;'`
* Create the configuration `kubectl apply -f manifests/config.yaml` 
* Deploy the app `kubectl apply -f manifests/web.yaml` 
* Deploy the ingress `kubectl apply -f manifests/ingress.yaml`
    * Wait for the certificate to be issued then go to the URL 
      https://hello-spring.k8s.kakuom.com to view your app  