package ju.ma.api

import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import ju.ma.ApiApplication
import ju.ma.controllers.CustomErrorModel
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest(
    classes = [ApiApplication::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = [
        "error.stack_trace=true"
    ]
)
@AutoConfigureEmbeddedDatabase(beanName = "dataSource")
class CustomErrorControllerTest(
    @LocalServerPort private val port: Int
) {
    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @Test
    fun `Returns custom error message as json`() {
        val response = this.restTemplate.getForObject(
            "http://localhost:$port/path/to/404/",
            CustomErrorModel::class.java
        )

        assertThat(response.error).isEqualTo("Not Found")
        assertThat(response.status).isEqualTo(404)
    }

    @Test
    fun `Returns custom error message as html`() {
        val headers = HttpHeaders()
        headers.accept = mutableListOf(MediaType.TEXT_HTML)

        val response = this.restTemplate.exchange(
            "http://localhost:$port/path/to/404/",
            HttpMethod.GET,
            HttpEntity("parameters", headers),
            String::class.java
        )

        assertThat(response.headers.contentType.toString()).contains(MediaType.TEXT_HTML_VALUE)
        assertThat(response.body).contains("Not Found")
    }
}
