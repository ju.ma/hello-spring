INSERT INTO courses (id, active, name) VALUES (nextval('course_sequence'), true, 'Computer Science 101');
INSERT INTO courses (id, active, name) VALUES (nextval('course_sequence'), false, 'Java OOP');

INSERT INTO students (id, name, course_id) VALUES (nextval('student_sequence'), 'Peter Pan', 1);
INSERT INTO students (id, name, course_id) VALUES (nextval('student_sequence'), 'John Snow', 2);

