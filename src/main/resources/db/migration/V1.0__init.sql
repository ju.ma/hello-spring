create sequence course_sequence start 1 increment 1;

create table courses (
    id int8 not null,
    active boolean,
    name varchar(255),
    start_date timestamp,
    primary key (id)
);

create sequence student_sequence start 1 increment 1;

create table students (
    id int8 not null,
    name varchar(255),
    start_date timestamp,
    course_id int8,
    primary key (id)
);

alter table if exists students add constraint course_students_course_id foreign key (course_id) references courses;