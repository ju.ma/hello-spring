package ju.ma.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.Instant
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "students")
class Student(
    var name: String,
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "students-seq-generator")
    @SequenceGenerator(name = "students-seq-generator", sequenceName = "student_sequence", allocationSize = 1)
    var id: Long = 0,
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "course_id")
    @JsonIgnore
    var course: Course? = null,
    var startDate: Instant = Instant.now()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Student) return false
        return id != 0L && id == other.id
    }

    /**
     * Since we cannot rely on a natural identifier for equality checks, we need to use
     * the entity identifier instead. However in order to ensure equality is consistent
     * across all entity state transitions, we return a static hashCode.
     * An entity must be equal to itself across all JPA object states
     *      - transient, attached, detached, removed
     */
    override fun hashCode(): Int {
        return 13
    }
}
