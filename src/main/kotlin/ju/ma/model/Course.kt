package ju.ma.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.Instant
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "courses")
class Course(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "course-seq-generator")
    @SequenceGenerator(name = "course-seq-generator", sequenceName = "course_sequence")
    var id: Long = 0,
    var name: String? = null,
    var active: Boolean = false,
    var startDate: Instant = Instant.now(),
    @JsonIgnore
    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY, cascade = [CascadeType.ALL], orphanRemoval = true)
    var students: MutableSet<Student> = mutableSetOf()
) {
    fun addStudent(job: Student) {
        students.add(job)
        job.course = this
    }

    fun removeStudent(job: Student) {
        students.remove(job)
        job.course = null
    }
}
