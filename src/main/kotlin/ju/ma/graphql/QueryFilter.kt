package ju.ma.graphql

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort

class QueryFilter(
    private var page: Int? = 0,
    private var size: Int? = 10,
    var sort: Array<KeyValuePair>? = null
) {

    fun create(): PageRequest {
        val sorted = sort ?: arrayOf()
        val sort: Sort = Sort.by(sorted.map { Sort.Order(Sort.Direction.fromString(it.value), it.key) })
        return PageRequest.of(page ?: 0, size ?: 10, sort)
    }
}

class KeyValuePair(
    var key: String,
    var value: String
)
