package ju.ma.graphql.course

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import java.time.Instant
import javax.persistence.EntityNotFoundException
import ju.ma.model.Course
import ju.ma.model.Student
import ju.ma.repository.CourseRepository
import org.springframework.stereotype.Component

@Component
class CourseMutation(private val repo: CourseRepository) : GraphQLMutationResolver {
    fun createCourse(course: CourseInput): Course {
        return repo.save(course.toCourse())
    }

    fun deleteCourse(id: Long): Course {
        val entity = repo.findById(id)
        if (entity.isPresent) {
            repo.delete(entity.get())
            return entity.get()
        }
        throw EntityNotFoundException("Unable to find an entity with the ID: $id")
    }
}

data class CourseInput(
    var name: String? = null,
    var active: Boolean = true,
    var startDate: Instant = Instant.now(),
    var students: MutableList<StudentInput> = mutableListOf()
) {
    fun toCourse(): Course {
        val course = Course(
            name = name,
            active = active,
            startDate = startDate
        )
        students.map { course.addStudent(it.toStudent()) }
        return course
    }
}

data class StudentInput(
    var name: String,
    var startDate: Instant = Instant.now()
) {
    fun toStudent(): Student {
        return Student(
            name = name,
            startDate = startDate
        )
    }
}
