package ju.ma.graphql.course

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import ju.ma.graphql.QueryFilter
import ju.ma.model.Course
import ju.ma.repository.CourseRepository
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component
class CourseQuery(private val repo: CourseRepository) : GraphQLQueryResolver {
    fun courses(filter: QueryFilter?): Page<Course> {
        val f: QueryFilter = filter ?: QueryFilter()
        return repo.findAll(f.create())
    }

    fun course(id: Long): Course = repo.findById(id).get()
}
