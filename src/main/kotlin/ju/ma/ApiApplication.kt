package ju.ma

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@SpringBootApplication
@EnableJpaRepositories("ju.ma.repository")
@EntityScan("ju.ma.model")
class ApiApplication

fun main(args: Array<String>) {
    runApplication<ApiApplication>(*args)
}
