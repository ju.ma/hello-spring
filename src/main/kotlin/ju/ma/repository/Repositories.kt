package ju.ma.repository

import ju.ma.model.Course
import ju.ma.model.Student
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = "courses", path = "courses")
interface CourseRepository : PagingAndSortingRepository<Course, Long>

@RepositoryRestResource(collectionResourceRel = "students", path = "students")
interface StudentRepository : PagingAndSortingRepository<Student, Long>
