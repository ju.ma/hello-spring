# Getting Started

* Build the app locally by running `./gradlew build`. This will create
  the jar at the location `build/libs/hello-spring-<version>-SNAPSHOT.jar`
  You can execute the jar manually, but you will have to provide all the 
  required parameters on the command line. It's easier to run with docker
* To run with docker execute `docker-compose up` or `docker-compose up -d` 
  to run as a daemon in the background
* A rest api is available at the root http://localhost:8080
* For instructions on how to deploy in kubernetes have a look at [manifests/README.md](manifests/README.md)
  
# GraphQl

You can navigate the graphQL api at http://localhost:8080/graphiql and 
issue queries such as:

```graphql
query FindCourses {
  courses {
    content {
      id
      name
      startDate
      active
      students {
        id
        name
        startDate
      }
    }
  }
}
```

